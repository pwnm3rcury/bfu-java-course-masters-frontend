import {Injectable} from '@angular/core';

import {Observable, of, throwError} from 'rxjs';
import {HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import {catchError, tap, map} from 'rxjs/operators';

// Interfaces
import {Student} from './app/students/student';
import {StudyProgram} from './app/study-programs/study-program';
import {Faculty} from './app/faculties/faculty';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'}),
};

// API Endpoints for Students
const studentsURL = 'http://localhost:7777/students/';

// API Endpoints for Student Programs
const studyProgramsURL = 'http://localhost:7777/study_programs';

// API Endpoints for Student Programs
const facultiesURL = 'http://localhost:7777/faculties';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) {

  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // Logs can be send to remote infrastructure.
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  // ----------------------------------------------------------------------
  // Students
  // ----------------------------------------------------------------------

  // Get all the Students
  getStudents(): Observable<Student[]> {
    return this.http.get<Student[]>(studentsURL)
      .pipe(
        tap(Student => console.log('fetched products')),
        catchError(this.handleError('getStudents', []))
      );
  }

  // Get a particular Student
  getStudent(id: number): Observable<Student> {
    const url = `${studentsURL}/${id}`;
    return this.http.get<Student>(url).pipe(
      tap(_ => console.log(`fetched product id=${id}`)),
      catchError(this.handleError<Student>(`getStudent id=${id}`))
    );
  }

  // Add a student
  addStudent(student): Observable<Student> {
    return this.http.post<Student>(studentsURL, student, httpOptions).pipe(
      tap((student: Student) => console.log(`added student w/ id=${student.id}`)),
      catchError(this.handleError<Student>('addStudent'))
    );
  }

  // Update a Student
  updateStudent(id: number, student): Observable<any> {
    const url = `${studentsURL}/${id}`;
    return this.http.put(url, student, httpOptions).pipe(
      tap(_ => console.log(`updated product id=${id}`)),
      catchError(this.handleError<any>('updateProduct'))
    );
  }

  // Delete a Student
  deleteStudent(id: number): Observable<Student> {
    const url = `${studentsURL}/${id}`;
    return this.http.delete<Student>(url, httpOptions).pipe(
      tap(_ => console.log(`deleted student id=${id}`)),
      catchError(this.handleError<Student>('deleteStudent'))
    );
  }

  // ----------------------------------------------------------------------
  // Student Programs
  // ----------------------------------------------------------------------

  // Get Student Programs
  getStudyPrograms(): Observable<StudyProgram[]> {
    return this.http.get<StudyProgram[]>(studyProgramsURL)
      .pipe(
        tap(StudyProgram => console.log('fetched studyPrograms', StudyProgram)),
        catchError(this.handleError('getStudyPrograms', []))
      );
  }

  // Get a particular Study Program
  getStudyProgram(id: number): Observable<StudyProgram> {
    const url = `${studyProgramsURL}/${id}`;
    return this.http.get<StudyProgram>(url).pipe(
      tap(_ => console.log(`fetched StudyProgram id=${id}`)),
      catchError(this.handleError<StudyProgram>(`getStudyProgram id=${id}`))
    );
  }

  // Add a Study Program.
  addStudyProgram(studyProgram): Observable<StudyProgram> {
    console.log(studyProgram);
    return this.http.post<StudyProgram>(studyProgramsURL, studyProgram, httpOptions).pipe(
      tap((studyProgram: StudyProgram) => console.log(`added studyProgram w/ id=${studyProgram.id}`)),
      catchError(this.handleError<StudyProgram>('addStudyProgram'))
    );
  }

  // Update a StudyProgram
  updateStudyProgram(id: number, studyProgram): Observable<StudyProgram> {
    const url = `${studyProgramsURL}/${id}`;
    return this.http.put(url, studyProgram, httpOptions).pipe(
      tap(_ => console.log(`updated StudyProgram id=${id}`)),
      catchError(this.handleError<any>('updateStudyProgram'))
    );
  }

  // Delete a StudyProgram
  deleteStudyProgram(id: number): Observable<StudyProgram> {
    const url = `${studyProgramsURL}/${id}`;
    return this.http.delete<StudyProgram>(url, httpOptions).pipe(
      tap(_ => console.log(`deleted StudyProgram id=${id}`)),
      catchError(this.handleError<StudyProgram>('deleteStudyProgram'))
    );
  }

  // ----------------------------------------------------------------------
  // Faculties
  // ----------------------------------------------------------------------

  // Get the faculties
  getFaculties(): Observable<Faculty[]> {
    return this.http.get<Faculty[]>(facultiesURL)
      .pipe(
        tap(Faculty => console.log('fetched faculties', Faculty)),
        catchError(this.handleError('getFaculties', []))
      );
  }

  // Get a particular faculty
  getFaculty(id: number): Observable<Faculty> {
    const url = `${facultiesURL}/${id}`;
    return this.http.get<Faculty>(url).pipe(
      tap(_ => console.log(`fetched faculty id=${id}`)),
      catchError(this.handleError<Faculty>(`getFaculty id=${id}`))
    );
  }

  // Add a faculty.
  addFaculty(faculty): Observable<Faculty> {
    return this.http.post<Faculty>(facultiesURL, faculty, httpOptions).pipe(
      tap((faculty: Faculty) => console.log(`added faculty w/ id=${faculty.id}`)),
      catchError(this.handleError<Faculty>('addFaculty'))
    );
  }

  // Update a Faculty
  updateFaculty(id: number, faculty): Observable<Faculty> {
    const url = `${facultiesURL}/${id}`;
    return this.http.put(url, faculty, httpOptions).pipe(
      tap(_ => console.log(`updated faculty id=${id}`)),
      catchError(this.handleError<any>('updateFaculty'))
    );
  }

  // Delete a Faculty
  deleteFaculty(id: number): Observable<Faculty> {
    const url = `${facultiesURL}/${id}`;
    return this.http.delete<Faculty>(url, httpOptions).pipe(
      tap(_ => console.log(`deleted faculty id=${id}`)),
      catchError(this.handleError<Faculty>('deleteFaculty'))
    );
  }

}
