import { Component, OnInit } from '@angular/core';

import { StudyProgram } from "./study-program";

import { ApiService } from 'src/api.service';

@Component({
  selector: 'app-study-programs',
  templateUrl: './study-programs.component.html',
  styleUrls: ['./study-programs.component.css']
})
export class StudyProgramsComponent implements OnInit {

  displayedColumns: string[] = [
    'id',
    'name',
    'degree',
    'typeOfStudy',
    'lengthOfStudy',
    'faculty.name'
  ];

  data: StudyProgram[] = [];

  isLoadingResults = true;

  constructor(private api: ApiService) { }

  ngOnInit() {
    this.api.getStudyPrograms()
    .subscribe(res => {
      this.data = res;
      console.log(this.data);
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
  }

}
