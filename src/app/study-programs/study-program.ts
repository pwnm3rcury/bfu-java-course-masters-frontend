export class StudyProgram {
    id: number;
    name: string;
    degree: string;
    typeOfStudy: number;
    lengthOfStudy: string;
    description: string;
    faculty: {
        id: number,
        name: string
    };
}
