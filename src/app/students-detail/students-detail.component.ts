import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../api.service';
import { Student } from '../students/student';

@Component({
  selector: 'app-students-detail',
  templateUrl: './students-detail.component.html',
  styleUrls: ['./students-detail.component.css']
})
export class StudentsDetailComponent implements OnInit {

  student: Student = {
    id: null,
    firstName: null,
    lastName: null,
    studentNumber: null,
    yearOfStudy: null,
    formOfEducation: null,
    studyProgram: {
      id: null,
      name: null,
    }
  };

  isLoadingResults = true;
  panelOpenState = false;

  constructor(private route: ActivatedRoute, private api: ApiService, private router: Router) { }

  getStudentDetails(id: number) {
    this.api.getStudent(id)
      .subscribe(data => {
        this.student = data;
        console.log(this.student);
        this.isLoadingResults = false;
      });
  }

  deleteStudent(id: number) {
    this.isLoadingResults = true;
    this.api.deleteStudent(id)
      .subscribe(res => {
          this.isLoadingResults = false;
          this.router.navigate(['/students']);
        }, (err) => {
          console.log(err);
          this.isLoadingResults = false;
        }
      );
  }

  ngOnInit() {
    this.getStudentDetails(this.route.snapshot.params['id']);
  }

}
