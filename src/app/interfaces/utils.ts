export interface FormOfEducation {
  value: number,
  viewValue: string
};

export interface Year {
  value: number,
  viewValue: string
};