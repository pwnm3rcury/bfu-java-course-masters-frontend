import { Component } from '@angular/core';

@Component({
    selector: 'dialog-student-successfully-added',
    templateUrl: 'dialog-student-successfully-added.html',
  })
  export class DialogStudentSuccessfullyAdded {}