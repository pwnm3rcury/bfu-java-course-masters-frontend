import { Component, OnInit } from '@angular/core';
import { StudyProgram } from "../study-programs/study-program";

import { ActivatedRoute, Router } from '@angular/router';

import { ApiService } from 'src/api.service';
import { FormOfEducation } from "../interfaces/utils";

@Component({
  selector: 'app-study-programs-detail',
  templateUrl: './study-programs-detail.component.html',
  styleUrls: ['./study-programs-detail.component.css']
})
export class StudyProgramsDetailComponent implements OnInit {

  studyProgram: StudyProgram = {
    id: null,
    name: null,
    degree: null,
    typeOfStudy: null,
    lengthOfStudy: null,
    description: null,
    faculty: {
      id: null,
      name: null
    },
  };

  isLoadingResults = true;
  panelOpenState = false;

  constructor(private route: ActivatedRoute, private api: ApiService, private router: Router) { }

  getStudyProgram(id: number) {
    this.api.getStudyProgram(id)
      .subscribe(data => {
        this.studyProgram = data;
        console.log(this.studyProgram);
        this.isLoadingResults = false;
      });
  }

  deleteStudyProgram(id: number) {
    this.isLoadingResults = true;
    this.api.deleteStudyProgram(id)
      .subscribe(res => {
        console.log(res);
        this.isLoadingResults = false;
        this.router.navigate(['/studyPrograms']);
      }, (err) => {
        console.log(err);
        this.isLoadingResults = false;
      }
      );
  }

  ngOnInit() {
    this.getStudyProgram(this.route.snapshot.params['id']);
  }

}
