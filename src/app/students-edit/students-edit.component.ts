import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';

import { ApiService } from '../../api.service';
import { Student } from '../students/student';
import { StudyProgram } from "../study-programs/study-program";
import { DialogStudentSuccessfullyAdded } from "../dialogs/dialog-student-successfully-added.component"

export interface Year {
  value: number,
  viewValue: string
}

export interface formOfEducation {
  value: number,
  viewValue: string
}

@Component({
  selector: 'app-students-edit',
  templateUrl: './students-edit.component.html',
  styleUrls: ['./students-edit.component.css']
})
export class StudentsEditComponent implements OnInit {

  studentForm: FormGroup;

  firstName: string = '';
  lastName: string = '';
  studentNumber: number = null;
  yearOfStudy: number = null;
  formOfEducation: number = null;
  studyProgramId: number = null;

  studyPrograms: StudyProgram[] = [];

  years: Year[] = [
    { value: 1, viewValue: '1' },
    { value: 2, viewValue: '2' },
    { value: 3, viewValue: '3' },
    { value: 4, viewValue: '4' },
    { value: 5, viewValue: '5' },
    { value: 6, viewValue: '6' }
  ];

  formOfEducations: formOfEducation[] = [
    { value: 1, viewValue: 'Редовно' },
    { value: 2, viewValue: 'Задочно' }
  ];

  student: Student = {
    id: null,
    firstName: null,
    lastName: null,
    studentNumber: null,
    yearOfStudy: null,
    formOfEducation: null,
    studyProgram: {
      id: null,
      name: null,
    }
  };

  isLoadingResults = true;

  // Snackbar timeout
  durationInSeconds = 5;

  constructor(
    private route:
    ActivatedRoute,
    private router: Router,
    private api: ApiService,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar
    ) {

    this.studentForm = this.formBuilder.group({
      'firstName': [this.firstName, Validators.compose([
        Validators.required,
        Validators.maxLength(255),
        Validators.minLength(5),
      ])],
      'lastName': [this.lastName, Validators.required],
      'studentNumber': [this.studentNumber, Validators.required],
      'yearOfStudy': [this.yearOfStudy, Validators.required],
      'formOfEducation': [this.formOfEducation, Validators.required],
      'studyProgram': this.formBuilder.group({
        'id': [this.studyProgramId, Validators.required]
      })
    });

  }

  openSnackBar() {
    this.snackBar.openFromComponent(DialogStudentSuccessfullyAdded, {
      duration: this.durationInSeconds * 1000,
      panelClass: ['panel-success']
    });
  }

  getStudentDetails(id: number) {
    this.api.getStudent(id)
      .subscribe(data => {
        this.student = data;
        console.log(this.student);
        this.isLoadingResults = false;
      });
  }

  ngOnInit() {

    // Getting the programs
    this.api.getStudyPrograms().subscribe(studyPrograms => {
      this.studyPrograms = studyPrograms;
    });

    // Getting the current Student
    this.getStudentDetails(this.route.snapshot.params['id']);
  }

  onFormSubmit(form: NgForm) {
    this.isLoadingResults = true;
    this.api.updateStudent(this.student.id, form)
      .subscribe(res => {
        this.openSnackBar();
        this.isLoadingResults = false;
        this.router.navigate(['/students/details/', this.student.id]);
      }, (err) => {
        this.isLoadingResults = false;
      });
  }


}
