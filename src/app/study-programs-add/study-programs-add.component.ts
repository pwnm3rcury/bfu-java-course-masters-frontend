import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { ApiService } from '../../api.service';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';

import { Faculty } from "../faculties/faculty";
import { DialogStudentSuccessfullyAdded } from "../dialogs/dialog-student-successfully-added.component"

import { Year, FormOfEducation } from "../interfaces/utils";

@Component({
  selector: 'app-study-programs-add',
  templateUrl: './study-programs-add.component.html',
  styleUrls: ['./study-programs-add.component.css']
})
export class StudyProgramsAddComponent implements OnInit {

  formOfEducations: FormOfEducation[] = [
    { value: 1, viewValue: 'Редовно' },
    { value: 2, viewValue: 'Задочно' }
  ];

  studyProgram: FormGroup;

  id: number = null;
  name: string = null;
  degree: string = null;
  typeOfStudy: number = null;
  lengthOfStudy: string = null;
  description: string = null;
  faculty_id: number = null;

  faculties: Faculty[] = [];

  years: Year[] = [
    { value: 1, viewValue: '1' },
    { value: 2, viewValue: '2' },
    { value: 3, viewValue: '3' },
    { value: 4, viewValue: '4' },
    { value: 5, viewValue: '5' },
    { value: 6, viewValue: '6' }
  ];

  isLoadingResults = false;
  showError = false;

  durationInSeconds = 5;

  constructor(private router: Router, private api: ApiService, private formBuilder: FormBuilder, private snackBar: MatSnackBar) {
    this.studyProgram = this.formBuilder.group({
      'name': [this.name, Validators.required],
      'degree': [this.degree, Validators.required],
      'typeOfStudy': [this.typeOfStudy, Validators.required],
      'lengthOfStudy': [this.lengthOfStudy, Validators.required],
      'description': [this.description, Validators.required],
      'faculty': this.formBuilder.group({
        'id': [this.faculty_id, Validators.required]
      })
    });
  }

  ngOnInit() {
    this.api.getFaculties().subscribe(faculties => {
      this.faculties = faculties
    });
  }

  openSnackBar() {
    this.snackBar.openFromComponent(DialogStudentSuccessfullyAdded, {
      duration: this.durationInSeconds * 1000,
      panelClass: ['panel-success']
    });
  }

  onFormSubmit(form: NgForm) {
    this.isLoadingResults = true;
    this.api.addStudyProgram(form)
      .subscribe(res => {
        this.openSnackBar();
        this.isLoadingResults = false;
        this.router.navigate(['study-programs']);
      }, (err) => {
        this.showError = true;
        this.isLoadingResults = false;
      });
  }

}
