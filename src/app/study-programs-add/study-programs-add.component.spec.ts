import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudyProgramsAddComponent } from './study-programs-add.component';

describe('StudyProgramsAddComponent', () => {
  let component: StudyProgramsAddComponent;
  let fixture: ComponentFixture<StudyProgramsAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudyProgramsAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudyProgramsAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
