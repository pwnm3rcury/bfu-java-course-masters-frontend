import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Faculty } from "../faculties/faculty";
import { ApiService } from 'src/api.service';

@Component({
  selector: 'app-faculties-detail',
  templateUrl: './faculties-detail.component.html',
  styleUrls: ['./faculties-detail.component.css']
})
export class FacultiesDetailComponent implements OnInit {

  faculty: Faculty = {
    id: null,
    name: null
  };

  isLoadingResults = true;
  panelOpenState = false;
  
  constructor(private route: ActivatedRoute, private api: ApiService, private router: Router) { }

  getFacultyDetails(id: number) {
    this.api.getFaculty(id)
      .subscribe(data => {
        this.faculty = data;
        console.log(this.faculty);
        this.isLoadingResults = false;
      });
  }

  deleteFaculty(id: number) {
    this.isLoadingResults = true;
    this.api.deleteFaculty(id)
      .subscribe(res => {
          this.isLoadingResults = false;
          this.router.navigate(['/faculties']);
        }, (err) => {
          console.log(err);
          this.isLoadingResults = false;
        }
      );
  }

  ngOnInit() {
    this.getFacultyDetails(this.route.snapshot.params['id']);
  }

}
